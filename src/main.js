// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue';
import App from './App';
import router from './router';
import NProgress from 'nprogress'; // Progress 进度条
import 'nprogress/nprogress.css'; // Progress 进度条 样式
import {getToken} from '@/utils/token';
import iView, {
  Affix,
  Alert,
  Anchor,
  AnchorLink,
  AutoComplete,
  Avatar,
  BackTop,
  Badge,
  Breadcrumb,
  BreadcrumbItem,
  Button,
  ButtonGroup,
  Card,
  Carousel,
  CarouselItem,
  Cascader,
  Cell,
  CellGroup,
  Checkbox,
  CheckboxGroup,
  Circle,
  Col,
  Collapse,
  ColorPicker,
  Content,
  Divider,
  DatePicker,
  Dropdown,
  DropdownItem,
  DropdownMenu,
  Footer,
  Form,
  FormItem,
  Header,
  Icon,
  Input,
  InputNumber,
  Layout,
  Menu,
  MenuGroup,
  MenuItem,
  Sider,
  Submenu,
  Modal,
  Option,
  OptionGroup,
  Page,
  Panel,
  Poptip,
  Progress,
  Radio,
  RadioGroup,
  Rate,
  Row,
  Select,
  Slider,
  Spin,
  Split,
  Step,
  Steps,
  Switch,
  Table,
  Tabs,
  TabPane,
  Tag,
  Time,
  Timeline,
  TimelineItem,
  TimePicker,
  Tooltip,
  Transfer,
  Tree,
  Upload
} from 'iview';
import 'iview/dist/styles/iview.css';
import '@/styles/index.less'; // global css

Vue.config.productionTip = false;

Vue.component('i-badge', Badge);
Vue.component('i-affix', Affix);
Vue.component('i-alert', Alert);
Vue.component('i-anchor', Anchor);
Vue.component('i-anchor-link', AnchorLink);
Vue.component('i-auto-complete', AutoComplete);
Vue.component('i-avatar', Avatar);
Vue.component('i-back-top', BackTop);
Vue.component('i-badge', Badge);
Vue.component('i-breadcrumb', Breadcrumb);
Vue.component('i-breadcrumb-item', BreadcrumbItem);
Vue.component('i-button', Button);
Vue.component('i-button-group', ButtonGroup);
Vue.component('i-card', Card);
Vue.component('i-carousel', Carousel);
Vue.component('i-carousel-item', CarouselItem);
Vue.component('i-cascader', Cascader);
Vue.component('i-cell', Cell);
Vue.component('i-cell-group', CellGroup);
Vue.component('i-checkbox', Checkbox);
Vue.component('i-checkbox-group', CheckboxGroup);
Vue.component('i-circle', Circle);
Vue.component('i-col', Col);
Vue.component('i-collapse', Collapse);
Vue.component('i-color-picker', ColorPicker);
Vue.component('i-content', Content);
Vue.component('i-divider', Divider);
Vue.component('i-date-picker', DatePicker);
Vue.component('i-dropdown', Dropdown);
Vue.component('i-dropdown-item', DropdownItem);
Vue.component('i-dropdown-menu', DropdownMenu);
Vue.component('i-footer', Footer);
Vue.component('i-form', Form);
Vue.component('i-form-item', FormItem);
Vue.component('i-header', Header);
Vue.component('i-icon', Icon);
Vue.component('i-input', Input);
Vue.component('i-input-number', InputNumber);
Vue.component('i-layout', Layout);
Vue.component('i-menu', Menu);
Vue.component('i-menu-group', MenuGroup);
Vue.component('i-menu-item', MenuItem);
Vue.component('i-sider', Sider);
Vue.component('i-submenu', Submenu);
Vue.component('i-modal', Modal);
Vue.component('i-option', Option);
Vue.component('i-option-group', OptionGroup);
Vue.component('i-page', Page);
Vue.component('i-panel', Panel);
Vue.component('i-poptip', Poptip);
Vue.component('i-progress', Progress);
Vue.component('i-radio', Radio);
Vue.component('i-radio-group', RadioGroup);
Vue.component('i-rate', Rate);
Vue.component('i-row', Row);
Vue.component('i-select', Select);
Vue.component('i-slider', Slider);
Vue.component('i-spin', Spin);
Vue.component('i-split', Split);
Vue.component('i-step', Step);
Vue.component('i-steps', Steps);
Vue.component('i-switch', Switch);
Vue.component('i-table', Table);
Vue.component('i-tabs', Tabs);
Vue.component('i-tab-pane', TabPane);
Vue.component('i-tag', Tag);
Vue.component('i-time', Time);
Vue.component('i-timeline', Timeline);
Vue.component('i-timeline-item', TimelineItem);
Vue.component('i-time-picker', TimePicker);
Vue.component('i-tooltip', Tooltip);
Vue.component('i-transfer', Transfer);
Vue.component('i-tree', Tree);
Vue.component('i-upload', Upload);
Vue.use(iView);

router.beforeEach((to, from, next) => {
  NProgress.start(); // 开启Progress 开启一个进度条
  if (to.matched.some(record => record.meta.requiresAuth)) {
    if (getToken()) {
      next();
    } else {
      next({
        path: '/login',
        query: { redirect: to.fullPath }
      });
    }
  } else {
    next();
  }
});

router.afterEach(() => {
  NProgress.done(); // 结束Progress
});

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  components: { App },
  template: '<App/>'
});
