import { stringify } from 'qs';
import request from 'utils/request';

let BASE_URL = '';
// 开发环境
if (process.env.NODE_ENV === 'development') {
  BASE_URL = 'http://47.93.241.33:9103/api/v1/notice/announcement';
  // 编译环境
} else {
  // 测试环境
  if (process.env.type === 'test') {
    BASE_URL = 'http://47.93.241.33:9103/api/v1/notice/announcement';
  } else {
    // 预发环境
    // BASE_URL = 'https://test.ono.chat/api/v1/notice/announcement';
    // 正式环境
    BASE_URL = 'https://api.ono.chat/api/v1/notice/announcement';
  }
}

// process.env.NODE_ENV === 'development
console.log(process.env.NODE_ENV);

export async function queryAnnounceListData (params) {
  return request(
    `${BASE_URL}/queryNoticeList?${stringify({
      ...params
    })}`
  );
}

export async function addNewAnnounce (params) {
  console.log(params);
  return request(`${BASE_URL}/insertNewAnnounceInfo`, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json'
    },
    body: JSON.stringify({
      ...params
    })
  });
}

export async function queryAnnounceDetail (params) {
  return request(
    `${BASE_URL}/watchNoticeDetailInfo?${stringify({
      ...params
    })}`
  );
}

export async function editAnnounceDetail (params) {
  console.log(params);
  return request(`${BASE_URL}/editNoticeDetailInfo`, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json'
    },
    body: JSON.stringify({
      ...params
    })
  });
}

export async function offlineAnnounce ({ id }) {
  let formData = new FormData();
  await formData.set('id', id);
  for (var [key, value] of formData.entries()) {
    console.log(key, value);
  }
  return request(`${BASE_URL}/noticeDownLine`, {
    method: 'POST',
    body: formData
  });
}
