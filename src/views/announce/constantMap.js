export const ERR_OK = 0;
export const ANNOUNCE_METHOD = {
  1: '弹框',
  2: '列表',
  3: '弹框+列表'
};
export const ANNOUNCE_TYPE = {
  1: '活动',
  2: '周报',
  3: '公示'
};
export const ANNOUNCE_STATUS = {
  1: {
    label: '待上线',
    dotStatus: 'processing'
  },
  2: {
    label: '已上线',
    dotStatus: 'success'
  },
  3: {
    label: '已下线',
    dotStatus: 'error'
  }
};
export const BTT_TYPES = {
  1: '是',
  2: '否'
};

export const LANGUAGES = {
  1: '中文',
  2: '英文',
  3: '韩文'
};
