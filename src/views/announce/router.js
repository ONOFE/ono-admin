export default {
  path: '/announce',
  name: 'announce',
  redirect: 'announce/list',
  component: resolve => require(['@/layout/basicLayout'], resolve),
  meta: {
    icon: 'logo-rss',
    title: '公告管理'
  },
  children: [
    {
      path: 'list',
      name: 'announceList',
      meta: {
        icon: 'md-list-box',
        title: '公告列表'
      },
      component: resolve => require(['@/views/announce/list'], resolve)
    },
    {
      path: 'add',
      name: 'addAnnounce',
      meta: {
        icon: 'md-add',
        title: '新建公告'
      },
      component: resolve => require(['@/views/announce/add'], resolve)
    },
    {
      path: 'edit/:id/:announceMethod',
      name: 'editAnnounce',
      meta: {
        hideInMenu: true,
        showInBreadCrumb: true,
        title: '修改公告'
      },
      component: resolve => require(['@/views/announce/edit'], resolve)
    },
    {
      path: 'detail/:id/:announceMethod',
      name: 'announceDetail',
      meta: {
        hideInMenu: true,
        showInBreadCrumb: true,
        title: '公告详情'
      },
      component: resolve => require(['@/views/announce/detail'], resolve)
    }
  ]
};
