import AWS from 'aws-sdk';
import OSS from 'ali-oss';
export default {
  methods: {
    getFileExtension (fileName) {
      let fileExtension = fileName.substring(fileName.lastIndexOf('.') + 1);
      return fileExtension;
    },
    uniqueID () {
      return Math.random()
        .toString(36)
        .substr(2, 7);
    },
    uploadByLanguage (langCode, file) {
      // 国内用户传阿里云OSS，其他传AWS
      if (langCode === '1') {
        return this.uploadToOSS(file);
      } else {
        return this.uploadToS3(file);
      }
    },
    async uploadToOSSAndS3 (file) {
      // 上传到OSS和S3
      const OSSurl = await this.uploadToOSS(file);
      const S3Url = await this.uploadToS3(file);
      return { OSSurl, S3Url };
    },
    uploadToS3 (file) {
      return new Promise((resolve, reject) => {
        let s3bucket = new AWS.S3({
          accessKeyId: 'AKIAJXH4JO4HF3CWS26A',
          secretAccessKey: '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',
          region: 'ap-northeast-1' // 东京
        });
        let suffix = this.getFileExtension(file.name); // 获取文件扩展名
        let name = new Date().getTime() + this.uniqueID(); // 随机生成文件名
        let Key = name + '.' + suffix; // 文件名
        s3bucket.createBucket(function () {
          var params = {
            Bucket: 'ono-auth',
            Key,
            ContentType: file.type,
            Body: file,
            ACL: 'public-read'
          };
          s3bucket.upload(params, function (err, data) {
            if (err) {
              reject(err);
            }
            resolve(data.Location);
          });
        });
      });
    },
    uploadToOSS (file) {
      // accessKeyId： LTAIwXqbcRrO4de7
      // accessKeySecret ：7CXCcxWe8QqvJ87i4e3067oHfG7Kza
      // region:oss-cn-beijing
      // bucket : ono-notice
      let suffix = this.getFileExtension(file.name); // 获取文件扩展名
      let name = new Date().getTime() + this.uniqueID(); // 随机生成文件名
      let storeAs = name + '.' + suffix; // 命名空间
      let client = new OSS({
        region: 'oss-cn-beijing',
        accessKeyId: 'LTAIwXqbcRrO4de7',
        accessKeySecret: '7CXCcxWe8QqvJ87i4e3067oHfG7Kza',
        bucket: 'ono-notice'
        // stsToken: this.aliyunData.SecurityToken
      });
      return new Promise((resolve, reject) => {
        client
          .multipartUpload(storeAs, file)
          .then(({ res }) => {
            // console.log(res);
            resolve(res.requestUrls[0].replace(/\?.+/, ''));
          })
          .catch(function (err) {
            reject(err);
          });
      });
    }
  }
};
