export default {
  path: '/test',
  name: 'test',
  meta: {
    hideInMenu: true,
    notCache: true
  },
  component: resolve => require(['@/views/test'], resolve)
};
