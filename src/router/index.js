import Vue from 'vue';

import Router from 'vue-router';
import announceRoutes from '@/views/announce/router';
import testRoutes from '@/views/test/router';
// import { getToken } from '@/utils/token';

Vue.use(Router);

let children = [announceRoutes, testRoutes];

export const routes = [
  {
    path: '/login',
    name: 'login',
    meta: {
      title: '登录',
      hideInMenu: true,
      notCache: true
    },
    component: resolve => require(['@/views/login'], resolve)
  },
  {
    path: '/',
    name: 'index',
    meta: {
      requiresAuth: true,
      hideInMenu: true,
      notCache: true
    },
    component: resolve => require(['@/App'], resolve),
    children
  }
];

const router = new Router({
  routes,
  mode: 'hash'
});

export default router;
