import Cookies from 'js-cookie';

// cookies remove time
// 单位为一天（24h）
const expires = 3;

// Read cookie:
export function getToken () {
  return Cookies.get('auth-token');
}

// Default: Cookie is removed when the user closes the browser.
export function setToken (token) {
  return Cookies.set('auth-token', token, {expires});
}

// Delete cookie:
export function removeToken () {
  return Cookies.remove('auth-token');
}
