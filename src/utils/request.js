import fetch from 'isomorphic-fetch';
import { Notice } from 'iview';
require('es6-promise').polyfill();

function checkStatus (response) {
  if (response.status >= 200 && response.status < 300) {
    return response;
  }
  Notice.error({
    title: `请求错误 ${response.status}: ${response.url}`,
    desc: response.statusText
  });
  const error = new Error(response.statusText);
  error.response = response;
  throw error;
}

/**
 * Requests a URL, returning a promise.
 *
 * @param  {string} url       The URL we want to request
 * @param  {object} [options] The options we want to pass to "fetch"
 * @return {object}           An object containing either "data" or "err"
 */
export default function request (url, options) {
  const defaultOptions = {
    credentials: 'include'
  };
  const newOptions = { ...defaultOptions, ...options };
  if (newOptions.method === 'POST' || newOptions.method === 'PUT') {
    newOptions.headers = {
      Accept: 'application/json',
      // 'Content-Type': 'application/x-www-form-urlencoded; charset=utf-8',
      ...newOptions.headers
    };
  }

  return fetch(url, newOptions)
    .then(checkStatus)
    .then(response => response.json())
    .catch(error => {
      if (error.code) {
        Notice.error({
          title: error.name,
          desc: error.message
        });
      }
      if ('stack' in error && 'message' in error) {
        Notice.error({
          title: `请求错误: ${url}`,
          desc: error.message
        });
      }
      return error;
    });
}
