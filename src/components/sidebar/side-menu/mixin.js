export default {
  methods: {
    showTitle (item) {
      if (item.meta && item.meta.title) {
        return item.meta.title;
      } else {
        return item.name;
      }
    },
    showChildren (item) {
      return item.children && item.children.length !== 0;
    }
  }
};
