# ONO-Admin

> ONO 后台管理系统

## 功能

+ 公告管理

## 基本使用

``` bash
# 安装依赖
npm install

# 在 localhost:8080 启动开发服务
npm run dev

# 构建生产环境代码
npm run build

# 构建用于测试环境的代码
npm run build-test

# 构建分析
npm run build --report
```

